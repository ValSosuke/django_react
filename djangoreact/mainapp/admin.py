from django.contrib import admin
from mainapp.models import BlogCategory, BlogPost
admin.site.register(BlogCategory)
admin.site.register(BlogPost)
# Register your models here.
