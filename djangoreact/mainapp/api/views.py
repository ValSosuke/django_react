#from rest_framework.views import APIView
#from rest_framework.response import Response
from rest_framework import viewsets #Объединяет из generic CreateAPIView, ListAPIView, RetriveAPIView, DeleteAPIView и работа проводится в router
from ..models import BlogCategory, BlogPost
from .serializers import (
    BlogCategorySerializer, 
    BlogPostSerializer, 
    BlogPostListRetriveSerializer,
    BlogCategoryDetailSerializer,
)

''' Простое применение rest_framework   
class TestAPIView(APIView):
    def get(request, *arg, **kwargs):
        data = [{"id" : 1, "name" : "John"}, {"id" : 2, "name" : "Jo"}]
        return Response(data) 
'''
class BlogCategoryViewSet(viewsets.ModelViewSet):

    queryset = BlogCategory.objects.all() #QerySet для ответа
    serializer_class = BlogCategorySerializer # Serializer для сериализации 

    action_to_serializer = {
        'retrieve': BlogCategoryDetailSerializer,
    }

    def get_serializer_class(self):
        return self.action_to_serializer.get(
            self.action, 
            self.serializer_class
        )


class BlogPostViewSet(viewsets.ModelViewSet):

    queryset = BlogPost.objects.all() #QerySet для ответа
    serializer_class = BlogPostSerializer # Serializer для сериализации 

    action_to_serializer = {
        "list": BlogPostListRetriveSerializer,
        "retrive": BlogPostListRetriveSerializer
    }

    def get_serializer_class(self):
        return self.action_to_serializer.get(
            self.action, 
            self.serializer_class
        )