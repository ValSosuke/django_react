from django.urls import path
#from .views import TestAPIView
from rest_framework import routers
from .views import BlogCategoryViewSet, BlogPostViewSet

routers = routers.SimpleRouter()
routers.register('category', BlogCategoryViewSet, basename='category') #Добавление viewset в routers 
routers.register('post', BlogPostViewSet, basename='post')

urlpatterns = [
    #path('test-api/', TestAPIView.as_view(), name='test')
]

urlpatterns += routers.urls #Добавление routers для доступа